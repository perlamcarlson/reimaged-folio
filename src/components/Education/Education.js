import React, { Component } from 'react';
import classes from './Education.module.css';
import ScrollAnimation from 'react-animate-on-scroll';
import "animate.css/animate.min.css";
import { MdSchool } from 'react-icons/md';
import { MdWork } from 'react-icons/md';
import { FaSchool } from 'react-icons/fa';

class Education extends Component {
    render() {
        return (
            <div className={classes.box} id="education">
                <ScrollAnimation offset={0} animateIn="fadeInLeft" duration={2} animateOnce={true} initiallyVisible={true}>
                    <span className={classes.head}>MY JOURNEY</span>
                    <section className={classes.container}>
                        <div className={classes.container_content}>
                            <div className={classes.row}>
                                <div className={classes.row_md_12}>
                                    <div className={classes.timeline_centered}>
                                        <ScrollAnimation offset={0} animateIn="fadeInLeft" duration={2.4} animateOnce={true} initiallyVisible={true}>
                                            <article className={classes.timeline_entry}>
                                                <div className={`${classes.timeline_icon} ${classes.timeline_icon_5}`} >
                                                    <MdWork />
                                                </div>
                                                <div className={classes.label}>
                                                    <h2 >Technical Skills</h2>
                                                    <h6>Programming Languages</h6>
                                                    <ul>
                                                        <li>Python3</li>
                                                        <li>Javascript</li>
                                                        <li>SQL</li>
                                                        <li>HTML5</li>
                                                        <li>CSS</li>
                                                    </ul>
                                                </div>
                                            </article>
                                        </ScrollAnimation>
                                        <ScrollAnimation offset={0} animateIn="fadeInLeft" duration={2.4} animateOnce={true} initiallyVisible={true}>
                                            <article className={classes.timeline_entry}>
                                                <div className={classes.timeline_icon} >
                                                    <MdSchool />
                                                </div>
                                                <div className={classes.label}>
                                                    <h2 >Application Development Experience </h2>
                                                    <p>Completed Hack Reactor's Software Engineer Immersive program</p>
                                                    <p>Projects:</p>
                                                    <p> * Let's Do That</p>
                                                    <p> * Pirate's Booty</p>
                                    
                                                </div>
                                            </article>
                                        </ScrollAnimation>
                                        <ScrollAnimation offset={0} animateIn="fadeInLeft" duration={2.4} animateOnce={true} initiallyVisible={true}>
                                            <article className={classes.timeline_entry}>
                                                <div className={`${classes.timeline_icon} ${classes.timeline_icon_2}`} >
                                                    <FaSchool />
                                                </div>
                                                <div className={classes.label}>
                                                    <h2 >Professional Experience  </h2>
                                                    <p>Ashley Kiely Living- Professional Organizer Assistant <span>2021-2022</span></p>
                                                    {/* <p> * Worked directly with the CEO to organize and come up with plans to organize various spaces</p>
                                                    <p>including homes as well as warehouses for high profile companies with a high volume of valuable products.</p> */}
                                                    
                                                    <p>Wu Yee Children's Services- Family Advocate and Spanish Translator <span>2013-2014</span></p>
                                                    {/* <p>*Worked in several roles at a Headstart for Early Child Development including: </p>
                                                    <p> Family Advocate and On-Call Translator in native Spanish.</p>
                                                    <p>*Provided resources and educational support for families and teachers </p>
                                                    <p>including:workshops in both English and Spanish, parenting tips and </p>
                                                    <p>tricks, and free resources available to the community.</p> */}
                                                    
                                                </div>
                                            </article>
                                        </ScrollAnimation>

                                        <ScrollAnimation offset={0} animateIn="fadeInLeft" duration={2.4} animateOnce={true} initiallyVisible={true}>
                                            <article>
                                                <div className={`${classes.timeline_icon} ${classes.timeline_icon_4}`} >
                                                    <MdWork />
                                                </div>
                                                <div className={classes.label}>
                                                    <h2 >Education
</h2>
                                                    <p>Hack Reactor- Software Engineer Immersive Program <span>2022-2022</span></p>
                                                    <p>San Jose State University- BA in Child Psychology <span>2010-2012</span></p>
                                                </div>
                                                <div className={classes.timeline_entry_inner}><div className={classes.timeline_icon_3 || classes.color_none}></div></div>
                                            </article>
                                        </ScrollAnimation>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </ScrollAnimation>
            </div>

        )
    }
}

export default Education;

import React, { Component } from 'react'
import classes from './About.module.css';
import ScrollAnimation from 'react-animate-on-scroll';
import "animate.css/animate.min.css";

class About extends Component {
    render() {
        return (
            <div className={classes.box} id="about">
                <ScrollAnimation offset={0} animateIn="fadeInLeft" duration={2.4} animateOnce={true} initiallyVisible={true}>
                    <span className={classes.head}>ABOUT ME</span>
                    <h2 className={classes.heading}>Who Am I?</h2>
                    <div className={classes.About}>
                        <p> My name is <b>PERLA CARLSON</b> and I am currently finished school to become a Software Developer at <a target="_blank" href="https://www.hackreactor.com/"><b>Hack Reactor</b></a>.   </p>
                        <p className={classes.br}>I am a life long learner not afraid to tackle something new. Im constantly growing and constantly expanding my base of knowledge. 
                        </p>
                    </div>
                </ScrollAnimation>
            </div>
        )
    }
}

export default About;